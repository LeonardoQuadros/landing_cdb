#!/bin/bash
#
# Procedimento para update e deploy da versão da loja virtual.
# Use: sh deploy.sh

# Remove compiled assets, tmp and log
rake assets:clean
rake tmp:clear
rake log:clear

# Re-compilar os assets no branch production
rake assets:clobber
rake assets:precompile RAILS_ENV=production

# Confirgurar o banco de dados em produção
# host: endereco_data_base
# database: nome_data_base
# username: nome_usuario_data_base
# password: senha_data_base

# Configurar secret_key_base no arquivo 'config/secrets.yml'
rake secret RAILS_ENV=production
# Gerar o secret_key no ambiente de produção utilizando o 'rake secret'
# Substituir <%= ENV["SECRET_KEY_BASE"] %> pela key gerada
# production:
#   secret_key_base: <%= ENV["SECRET_KEY_BASE"] %>

# CONFIGURAÇÔES
git pull

rm -rf .bundle vendor/bundle
bundle install
bundle install --deployment

rake db:migrate RAILS_ENV=production
rake db:seed RAILS_ENV=production

chown -R machete:machete .

/etc/init.d/nginx -s reload

RAILS_ENV=production script/delayed_job start
RAILS_ENV=production script/delayed_job restart


# reset Banco
rake db:drop
rake db:create
rake db:migrate
rake db:seed

# Atualizar o Sitemap em config/sitemap.rb
# Corrigir o Host
# Adicionar as rotas caso necessário
# adicionar função para atualizar o sitemap via crontab
rake sitemap:refresh RAILS_ENV=production

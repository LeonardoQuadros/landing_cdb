class CreateContatos < ActiveRecord::Migration
  def change
    create_table :contatos do |t|
    	t.string :nome
        t.string :email
    	t.date :data_nascimento
    	t.string :telefone
        t.string :celular
        t.string :cpf
        t.string :cnpj
        t.string :rg
        t.string :nacionalidade
        t.string :profissao
    	t.integer :sexo
    	t.text :mensagem
    	t.string :remote_ip
    	t.string :user_agent
    	t.datetime :lido_em
        t.boolean :pre_cadastro

    	t.timestamps
    end
  end
end

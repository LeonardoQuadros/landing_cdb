# -*- encoding : utf-8 -*-
class AddColumnsToUsuarios < ActiveRecord::Migration
  def change
    add_column :usuarios, :nome, :string
    add_column :usuarios, :telefone, :string
    add_column :usuarios, :celular, :string
    add_column :usuarios, :super_admin, :boolean, default: false
    add_column :usuarios, :ativo, :boolean, null: false, default: true
  end
end

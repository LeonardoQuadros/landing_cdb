# -*- encoding : utf-8 -*-
# Supported options: :resque, :sidekiq, :delayed_job, :queue_classic, :torquebox, :backburner, :que, :sucker_punch
Devise::Async.backend = :delayed_job
$(document).ready(function() {

  $('#menu').on('affix.bs.affix', function(e) {
    $(this).addClass('navbar-shrink');
  });

  $('#menu').on('affixed-top.bs.affix', function(e) {
    $(this).removeClass('navbar-shrink');
  });


});

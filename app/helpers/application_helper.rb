module ApplicationHelper

  def resource_name
    :cliente
  end

  def resource
    @resource ||= Cliente.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:cliente]
  end

  def pagination_footer( object_list, parameters = {} )

    parameters[:per_page] = WillPaginate.per_page if parameters[:per_page].blank?
    options = [
      [parameters[:per_page], parameters[:per_page]],
      [(parameters[:per_page]*2.5).to_i, (parameters[:per_page]*2.5).to_i],
      [(parameters[:per_page]*5).to_i, (parameters[:per_page]*5).to_i],
      [I18n.t('will_paginate.all'), object_list.count]
    ]

    html = []
    html << '<div id="pagination">'
    html << '<span class="exibir">' + I18n.t('will_paginate.show_label') + '</span>'
    html << select_tag(:per_page, options_for_select(options, params[:per_page]), local_url: local_url(request, params), class: "form-control", style: "width: 100px;")
    html << will_paginate(object_list)
    html << '</div>'

    return html.join('').html_safe
  end

  def local_url(request, params)
    request.protocol + request.host_with_port + request.path + '?' + params.select{|k,v| k != :per_page}.collect {|k, v| "#{k}=#{v}"}.join('&')
  end

  def selecionar_checkbox_th(to_select = '.cid', select_all = '#check_all')
    check_box_tag select_all, '', false, { onclick: "checkOrUncheckAll( $(this).is(':checked'), '#{to_select}' )", class: :check_all }
  end

  def check_all_td( id, checked = false )
    check_box_tag "cid[]", id, checked, { :id => "cb#{id}", :class => "cid" }
  end

  def full_title(page_title = '')
    base_title = Settings['CONFIGURACOES.title']

    return base_title if page_title.empty?
    return "#{page_title} | #{base_title}"
  end

end

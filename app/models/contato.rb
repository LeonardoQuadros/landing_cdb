# -*- encoding : utf-8 -*-
class Contato < ActiveRecord::Base

  validates :nome, :email, :cidade, :origem, :capital_disponivel, :telefone, presence: true

  belongs_to :estado

  scope :ordenados, -> {order('contatos.created_at ASC')}
  scope :nao_lidos, -> {where('contatos.lido_em IS NULL')}

  def codigo_to_s
    "#{self.id}"
  end

  def self.to_select
    Contato.ordenados.collect { |u| [u.nome, u.id] }
  end

  def self.to_select_cidade estado
    Cidade.where(estado_id: estado.id).collect { |u| [u.nome, u.nome] }
  end

  def self.to_select_origem
    [
      ["Linkedin", "Linkedin"],
      ["Recebeu Ligação", "Recebeu Ligação"],
      ["Recebeu Mensagem", "Recebeu Mensagem"],
      ["E-mail / Marketing", "E-mail / Marketing"],
      ["Google", "Google"],
      ["Facebook", "Facebook"],
      ["YouTube", "YouTube"],
      ["Twitter", "Twitter"],
      ["Feiras", "Feiras"]
    ]
  end

  def self.to_select_capital
    [
      ["Até R$ 50.000,00", "Até R$ 50.000,00"],
      ["De R$ 50.000,00 Até 100.000,00", "De R$ 50.000,00 Até 100.000,00"],
      ["De R$ 100.000,00 Até 150.000,00", "De R$ 100.000,00 Até 150.000,00"],
      ["De R$ 150.000,00 Até 200.000,00", "De R$ 150.000,00 Até 200.000,00"],
      ["De R$ 200.000,00 Até 250.000,00", "De R$ 200.000,00 Até 250.000,00"],
      ["De R$ 250.000,00 Até 300.000,00", "De R$ 250.000,00 Até 300.000,00"],
      ["De R$ 300.000,00 Até 350.000,00", "De R$ 300.000,00 Até 350.000,00"],
      ["De R$ 350.000,00 Até 400.000,00", "De R$ 350.000,00 Até 400.000,00"],
      ["De R$ 400.000,00 Até 450.000,00", "De R$ 400.000,00 Até 450.000,00"],
      ["De R$ 450.000,00 Até 500.000,00", "De R$ 450.000,00 Até 500.000,00"],
      ["Acima de R$ 500.000,00", "Acima de R$ 500.000,00"]
    ]
  end


  def self.media_dia data
      inicio = data.beginning_of_day
      fim = data.beginning_of_day + 1.day
      contatos = Contato.where('created_at BETWEEN ? AND ?', inicio.to_s(:db), fim.to_s(:db)).size
  end
  
end

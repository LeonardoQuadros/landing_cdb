# -*- encoding : utf-8 -*-
class Cidade < ActiveRecord::Base

  belongs_to :estado

  validates :nome, presence: true

  def nome_completo
    "#{nome} - #{estado.acronimo.upcase}"
  end

  def self.to_select
    Cidade.all.collect { |u| [u.nome, u.id] }
  end


end

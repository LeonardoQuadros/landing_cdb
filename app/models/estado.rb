# -*- encoding : utf-8 -*-
class Estado < ActiveRecord::Base

  has_many :cidades

  validates :nome, :acronimo, uniqueness: true

  def nome_completo
  	"#{self.acronimo} - #{self.nome}"
  end

  def self.to_select
    Estado.all.collect { |u| [u.nome_completo, u.id] }
  end

end

# -*- encoding : utf-8 -*-
class A::Usuarios::PasswordsController < Devise::PasswordsController

  # layout false, only: [:new, :create]
  # layout 'site', only: [:edit, :update]

  layout :layout_by_resource

  protected

  def after_sending_reset_password_instructions_path_for(resource_name)
    new_a_usuario_session_path
  end

  def layout_by_resource
    if devise_controller? && (action_name == "new" or action_name == "create")
      false
    else
      "application"
    end
  end

end
# -*- encoding : utf-8 -*-
module Api
  module V1
    class CategoriasController < ApiController

      def index
        begin
          @categorias = Categoria.ordenados.ativos
        rescue Exception => e
        end

        unless @categorias.blank?
          render 'api/v1/categorias/index'
        else
          render json: {errors: 'Categorias não localizadas', status: 406}, status: 406
        end
      end

      def show
        begin
          @categoria = Categoria.find params[:id]
        rescue Exception => e
        end

        unless @categoria.blank?
          @produtos = @categoria.produtos.ativos.ordenados
          render 'api/v1/categorias/show'
        else
          render json: {errors: 'Categoria não localizada', status: 406}, status: 406
        end
      end

      # Adicionar uma Categoria
      def create
        @categoria = Categoria.new(categoria_params)
        if @categoria.save
          render json: @categoria
        else
          render json: {errors: @categoria.errors.full_messages}, status: 406
        end
      end

      private
        # Only allow a trusted parameter "white list" through.
        def categoria_params
          params.require(:categoria).permit(:titulo)
        end

    end
  end
end
